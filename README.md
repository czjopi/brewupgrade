# Script for upgrade packages installed by `brew`

## Download the script and make it executable

```bash
curl -s -o ~/brewupgrade.sh https://gitlab.com/czjopi/brewupgrade/-/raw/main/brewupgrade.sh
chmod u+x ~/brewupgrade.sh
```

## Run script

```bash
~/brewupgrade.sh
```
