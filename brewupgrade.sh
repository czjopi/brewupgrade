#!/bin/sh

ARCH=$(uname -m)

if [ -z "${BREW_BINARY}" ]; then
  if [ "${ARCH}" = "x86_64" ]; then
    BREW_BINARY='/usr/local/bin/brew'
  fi
  if [ "${ARCH}" = "arm64" ]; then
    BREW_BINARY='/opt/homebrew/bin/brew'
  fi
fi

${BREW_BINARY} update >/dev/null 2>&1

brew_cask_out=$(${BREW_BINARY} outdated --cask --quiet)

if [ -n "${brew_cask_out}" ]; then
  echo 'Casks for upgrade:'
  echo "${brew_cask_out}"
  for package in ${brew_cask_out}; do
    echo "Upgrading ${package}"
    ${BREW_BINARY} upgrade --cask "${package}"
  done
else
  echo 'Cask: nothing to do'
fi

brew_out=$(${BREW_BINARY} outdated --quiet)

if [ -n "${brew_out}" ]; then
  echo 'Packages for upgrade:'
  echo "${brew_out}"
  for package in ${brew_out}; do
    echo "Upgrading ${package}"
    "${BREW_BINARY}" upgrade "${package}"
  done
else
  echo 'Taps: nothing to do'
fi

if [ -n "${brew_cask_out}" ] || [ -n "${brew_out}" ]; then
  ${BREW_BINARY} cleanup
fi
